<?php
include 'settings.php';
//Read a web page and check for errors:

$result = get_web_page( "https://www.amazon.com/s/ref=nb_sb_noss_2?page=1&url=search-alias%3Daps&field-keywords=". str_replace(' ', '+', $_GET['k']) );

if ( $result['errno'] != 0 )
    exit('error: bad url, timeout, redirect loop');

if ( $result['http_code'] != 200 )
    exit('error: no page, no permissions, no service');

header( "HTTP/1.1 200 OK" );
exit($result['content']);