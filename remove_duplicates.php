<?php
include 'settings.php';

if(isset($_GET['reset'])){
	if(file_exists(DATA_FILE_PATH)) unlink(DATA_FILE_PATH);
	header('location: /');
}

if(file_exists(COMP_FILE_PATH)) unlink(COMP_FILE_PATH);
?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>CSV PARSE</title>

	<style>
		*{
			margin: 0;
			padding: 0;
		}

		#form{
			position: relative;
			background: #F00;
			padding: 10px;
			color: #FFF;
		}

		.loading #form:before{
			content: 'Processing...';
			text-align: center;
			color: #FFF;
			position: absolute;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			background: rgba(0,0,0,.6);
			z-index: 1;
			line-height: 54px;
			font-family: Arial;
			font-weight: 600;
			font-size: 14px;
			letter-spacing: 1px;
		}

		.input{
			height: 30px;
			font-weight: 600;
			font-size: 12px;
			color: #AAA;
			padding: 0 4px;
		}

		#stop_words{
			width: 450px;
		}

		#brands{
			width: 300px;
		}

		button{
			padding: 7px 15px;
			cursor: pointer;
		}

		.amazon{
			display: none;
		}

		.amazon.active{
			display: block;
		}

		.interface{
			display: none;
		}

		.interface.active{
			display: inline;
		}

		.download{
			display: none;
		}

		.success .download{
			display: inline;
			position: relative;
			z-index: 2;
		}
	</style>
</head>
<body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

<?php
if(isset($_FILES['data'])){
	move_uploaded_file( $_FILES['data']['tmp_name'], DATA_FILE_PATH);
}

if(file_exists(DATA_FILE_PATH)):

	$data = file (DATA_FILE_PATH);
	$clear_phrases = [];

	foreach ($data as &$item){
	    $item = trim($item);
	    if( ! in_array($item, $clear_phrases)) array_push($clear_phrases, $item);
    }

	sort($clear_phrases);

    $words = [];
    $clear_words = [];

    foreach ($clear_phrases as $phrase){
        foreach (explode(' ', $phrase) as $word){
            $word = trim($word);
            if( ! isset($clear_words[$word])) $clear_words[$word] = 0;
	        $clear_words[$word] ++;
        }
    }

	$fp = fopen(COMP_FILE_PATH . '_dub.csv',"wb");
	fwrite($fp, implode('
', $clear_phrases));
	fclose($fp);

	$clear_words_str = '';
	foreach ($clear_words as $word=>$num){
	    if($word = trim($word)) {
		    $clear_words_str .= ( $clear_words_str ? '
' : '' ) . "$word,$num";
	    }
    }

	$fp = fopen(COMP_FILE_PATH . '_words.csv',"wb");
	fwrite($fp, $clear_words_str);
	fclose($fp);

	e('success');

else:?>

	<form action="/remove_duplicates.php" method="post" enctype="multipart/form-data" accept="text/csv">
		<label for="data">
			File:
			<input onchange="$(this).closest('form').submit()" type="file" name="data">
		</label>
	</form>

<?php endif;?>

</body>
</html>