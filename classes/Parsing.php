<?php

class Parsing {

	public function auto($data, $brands=[], $products=[]){

		$result = [];
		$result_str = '';
		$good = [];
		$bad = [];
		$error = [];

		$depth = 3;

		for ($i=0; $i<count($data); $i++){
			$item = $data[$i];

			if($item[0] == 'phrase') continue;

			// Variables for pages of search
			$page_brands = [];
			$page_products = [];

			for ($d=1; $d < $depth+1; $d++){

				$page = get_web_page( "https://www.amazon.com/s/ref=nb_sb_noss_2?page=$d&url=search-alias%3Daps&field-keywords=" . str_replace( ' ', '+', $item[0] ) );
				if(($page['errno'] != 0) || ($page['http_code'] != 200)){
					$error[$i] = $page;
					continue;
				}

				// Create a DOM object
				$html = new simple_html_dom();

				// Load HTML from a string
				$html->load( $page['content'] );

				// Select table of search results of amazon
				$html = $html->getElementById( 'resultsCol' );

				$li_arr = $html->find( 'li.s-result-item' );
				for ($l=0; $l<count($li_arr); $l++) {
					$li = $li_arr[$l];

					if ( $html = $li->find( '.a-col-right', 0 ) ) {

						// Resume ads
						if($html->find('h5.s-sponsored-list-header')) continue;

						// Save product name if exists
						if(isset($li->attr['data-asin'])){
							$product_asin = trim($li->attr['data-asin']);
							if ( in_array( $product_asin, $products )) {
								$product_name = '-';
								if($product_name_html = $html->find('.a-row.a-spacing-small' , 0)){
									if($product_name_html = $product_name_html->find('.a-row.a-spacing-none a h2' , 0)){
										$product_name = $product_name_html->plaintext;
									}
								}
								array_push($page_products, "$product_name ($product_asin)");
							}
						}

						// Save brand name if exists or break
						if (
							($brand_name = $html->find( '.a-spacing-small', 0 )) &&
							($brand_name = $brand_name->find( '.a-row', 1 )) &&
							($brand_name = $brand_name->find( '.a-size-small', 1 ))
						) break;

						$brand_name = trim($brand_name->plaintext);
						if ( in_array( $brand_name, $brands ) && !in_array($brand_name, $page_brands) ) {
							array_push($page_brands, $brand_name);
						}
					}
				}

			}

			if($page_products || $page_brands){
				$item[1] = trim($item[1]);

				array_push($result, [
					$item[0],
					$item[1],
					$page_brands,
					$page_products
				]);

				$result_str .= "\n$item[0],$item[1],"
				               .implode(' / ', $page_brands)
				               .",".implode(' / ', $page_products);

				array_push($good, $i);
				continue;
			}
			array_push($bad, $i);
		}

		return (object)
		[
			'result' => (object)
			[
				'arr' => $result,
				'str' => $result_str,
			],
			'good' => $good,
			'bad' => $bad,
			'error' => $error
		];

	}

}