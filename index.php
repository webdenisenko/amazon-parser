<?php
include 'settings.php';
include 'test.php';

if(isset($_GET['reset'])){
	if(file_exists(DATA_FILE_PATH)) unlink(DATA_FILE_PATH);
	header('location: /');
}

if(file_exists(COMP_FILE_PATH)) unlink(COMP_FILE_PATH);
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CSV PARSE</title>

    <style>
        *{
            margin: 0;
            padding: 0;
        }

        #form{
            position: relative;
            background: #F00;
            padding: 10px;
            color: #FFF;
        }

        .loading #form:before{
            content: 'Processing...';
            text-align: center;
            color: #FFF;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: rgba(0,0,0,.6);
            z-index: 1;
            line-height: 54px;
            font-family: Arial;
            font-weight: 600;
            font-size: 14px;
            letter-spacing: 1px;
        }

        .input{
            height: 30px;
            font-weight: 600;
            font-size: 12px;
            color: #AAA;
            padding: 0 4px;
        }

        #stop_words{
            width: 450px;
        }

        #brands{
            width: 300px;
        }

        button{
            padding: 7px 15px;
            cursor: pointer;
        }

        .amazon{
            display: none;
        }

        .amazon.active{
            display: block;
        }

        .interface{
            display: none;
        }

        .interface.active{
            display: inline;
        }

        #key{
            display: inline-block;
            vertical-align: middle;
            margin-top: -5px;
            max-width: 200px;
            overflow: hidden;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .download{
            display: none;
        }

        .success .download{
            display: inline;
            position: relative;
            z-index: 2;
        }

        .inline{
            display: inline-block;
            vertical-align: top;
            width: 400px;
            margin-right: 15px;
        }

        .inline .input{
            display: block;
            width: 100% !important;
        }
    </style>
</head>
<body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

<?php
if(file_exists(DATA_FILE_PATH)):?>

    <audio id="success_audio">
        <source src="audio/notify.ogg" type="audio/ogg">
        <source src="audio/notify.mp3" type="audio/mpeg">
        <source src="audio/notify.m4r" type="audio/m4r">
    </audio>

    <audio id="error_audio">
        <source src="audio/error.ogg" type="audio/ogg">
        <source src="audio/erorr.mp3" type="audio/mpeg">
        <source src="audio/erorr.m4r" type="audio/m4r">
    </audio>

    <div id="form">
        <button onclick="location.href = '/?reset'" id="reset">X</button>
        <input class="input" id="stop_words" placeholder="Stop words">
        <span id="interface1" class="interface active">
            <button id="add">ADD</button>
            <button id="cancel">CANCEL</button>
            <span>Keyword: <b title="" id="key"></b></span>
            <div style="float:right">Left: <span id="count"></span>
                <button style="margin-left:25px;" class="auto">AUTO MODE</button>
                <button onclick="location.href = $(this).data('url')" class="download" style="margin-left:25px;" class="auto">DOWNLOAD</button>
            </div>
        </span>
        <span id="interface2" class="interface">
            <span class="inline">
                <input class="input" id="brands" placeholder="Brands" value="">
                <input class="input" id="products" placeholder="Products ASIN(s)" value="B00GR8KEMS, B01D2U3UOM, B01GW49ZRG, B06ZZRW8BL, B01NASPXSR, B06XZ2917T, B01DTMA0FK, B01N0XA9LL">
            </span>
            <button id="start">START</button>
            <div style="float:right">
                <button class="auto">BACK</button>
                <button onclick="location.href = $(this).data('url')" class="download" style="margin-left:25px;" class="auto">DOWNLOAD</button>
            </div>
        </span>
    </div>

    <iframe class="amazon" width="100%" height="540px" src="amazon.php?k=" frameborder="0"></iframe>
    <iframe class="amazon" width="100%" height="540px" src="" frameborder="0"></iframe>

    <script>
        var $words = $('#stop_words'),
            $add = $('#add'),
            $cancel = $('#cancel'),
            old_words = '',
            data = <?php

		    $data = file (DATA_FILE_PATH);

		    foreach ($data as &$item){
			    $item = explode(',', $item);
		    }

		    echo json_encode(utf8ize($data));
		    ?>;

        $(document).ready(function () {

            var notify_allow = Notification.permission.toLowerCase();

            switch ( notify_allow ) {
                case "denied":
                    alert('Внимание! Вы не включили уведомления в браузере.')
                    break;

                case "default":
                    Notification.requestPermission( function (permission) {
                        if( permission != "granted" ) return false;
                        var notify = new Notification("Great! I am test message :)");
                    } );
            }

            function show_message(title, text, icon, tag) {
                return notify_allow == 'granted' ? new Notification(title, {
                    tag : tag?tag:'default',
                    body : text,
                    icon : icon
                }) : alert(title+'\n'+text);
            }

            function has_stop_word(word, item) {
                if(word && item){

                    word = $.trim(word).toLowerCase();
                    item = item.toLowerCase();

                    return (item.indexOf(word+' ') > -1) ||
                        (item.indexOf(' '+word) == item.length - word.length-1) ||
                        (item.indexOf(' '+word+' ') > -1) ||
                        (item == word);
                }

                return false;
            }

            function has_stop_word_array(words, item) {
                if(words && item){

                    var has = false;
                    for(var i=0; i<words.length; i++){
                        if(has_stop_word(words[i], item)){
                            has = true;
                            break;
                        }
                    }
                    return has;

                }

                return false;
            }

            function update_iframe() {

                if(!$('.amazon.active').length){
                    $('.amazon:first').addClass('active').attr('src', 'amazon.php?k='+data[0][0])
                        .next().attr('src', 'amazon.php?k='+(data[1] ? data[1][0] : ''));
                }
                else{
                    $('.amazon').toggleClass('active');
                    $('.amazon:not(.active)').attr('src', 'amazon.php?k='+(data[1] ? data[1][0] : ''));
                }
            }

            function get_item(item_key, param) {
                if(!data.length) return false;
                param = param ? 1 : 0;

                if(!item_key) item_key = 0;
                return data[item_key] ? data[0][param] : false;
            }

            function send(add) {
                $.ajax({
                    url: 'controllers/manual.php',
                    type: 'POST',
                    data: {
                        'add':add ? 'T' : 'F',
                        'key':get_item(),
                        'num':get_item(0, 1)
                    },
                    complete: function () {
                        next_keys();
                    }
                });
            }

            function success_work(timing) {
                return $.ajax({
                    url: 'controllers/finish.php',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        'stop':$words.val(),
                    },
                    complete: function (res) {
                        $('#count').html(0);
                        $('#key').html('<small>(Empty)</small>');

                        if(res && res.responseJSON){
                            res = res.responseJSON;

                            if(res.status == 'success'){
                                $('.download').attr('data-url', res.data);
                                $('body').addClass('success');
                                $('#success_audio')[0].play();
                                return show_message('Completed!', 'Parsing successfully completed. You can download a archive.'+(timing?'\nTime: '+(timing/1000)+' seconds.':''));
                            }else if(res.status == 'empty'){
                                return show_message('Completed!', 'Words did not found...'+(timing?'\nTime: '+(timing/1000)+' seconds.':''));
                            }
                        }

                        $('#error_audio')[0].play();
                        show_message('Oops!', 'Something went wrong.');
                        console.error('AJAX:', res);
                    }
                });
            }

            var timing = 0;
            function auto_mode() {

                var step_length = 25,
                    streams = 4,
                    curr_streams = 0,
                    all_steps = 0,
                    curr_step = 0,
                    auto_mode_data = {
                        'brands': $('#brands').val().split(','),
                        'products': $('#products').val().split(','),
                        'data': []
                    },
                    clear_data = [],
                    stop_words = [],
                    processed = 0;
                    goods = 0;
                    ignored = 0;
                    errors = 0;

                // Remove whitespaces from brands
                $.each(auto_mode_data['brands'], function (b, brand) {
                    auto_mode_data['brands'][b] = $.trim(brand);
                });

                // Remove whitespaces from ASINs
                $.each(auto_mode_data['products'], function (b, product) {
                    auto_mode_data['products'][b] = $.trim(product);
                });

                // Filter empty stop words
                stop_words = $.grep($words.val().split(','), function (val) {
                    return val;
                });

                // Add brands to stop words
                stop_words = $.merge(stop_words, auto_mode_data['brands']);

                // Filter keywords by stop words
                clear_data = $.grep(data, function (data_val) {
                    return !has_stop_word_array(stop_words, data_val[0]);
                });

                // Count steps (pages)
                all_steps = Math.ceil((clear_data.length-1) / step_length);

                // Add prelaoder
                $('body').addClass('loading');

                function send_auto_mode() {

                    if(curr_streams >= streams) return false;

                    // If first start
                    if(!curr_step){
                        show_message('Start parsing...', 'The process is started. This may take about 5 minutes or more.')
                        timing = Date.now();
                    }

                    // If finish
                    if(curr_step == all_steps){
                        console.log(goods + ignored + errors, clear_data.length-1);
                        if(
                            curr_streams ||
                            (goods + ignored + errors != clear_data.length-1)
                        ) return false;
                        $('body').removeClass('loading');
                        return success_work(Date.now() - timing);
                    }

                    // Count keywords by step
                    auto_mode_data['data'] = $.grep(clear_data, function (a, b) {
                        return (b > curr_step*step_length) && (b < curr_step*step_length + (step_length+1));
                    });

                    // Send ajax query
                    $.ajax({
                        url: 'controllers/auto.php',
                        type: 'POST',
                        data: auto_mode_data,
                        dataType: 'json',
                        beforeSend: function () {
                            curr_streams ++;
                            curr_step ++;
                            send_auto_mode();
                        },
                        complete: function (res) {
                            if(res && res.responseJSON){
                                res = res.responseJSON;

                                if(res.status == 'success'){
                                    res = res.data;

                                    processed += auto_mode_data['data'].length;
                                    show_message(
                                        processed+' of '+(clear_data.length-1)+' successfully processed!',
                                        'added: '+ (goods += res.good.length) +'; ignored:'+(ignored += res.bad.length)+'; error:'+(errors += res.error.length)+';\n'+
                                        'Time: '+( (Date.now() - timing)/1000 )+' seconds'
                                    );

                                    curr_streams --;
                                    return send_auto_mode();
                                }
                            }

                            $('#error_audio')[0].play();
                            show_message('Oops!', 'Something went wrong.');
                            console.error('AJAX:', res);
                        }
                    });
                }send_auto_mode();

            }

            $('#count').html(data.length);
            function next_keys() {

                data = $.grep(data, function (a, b) {
                    return b;
                });

                if($words.val() != old_words){
                    old_words = $words.val();

                    $.each(old_words.split(','), function (k, word) {

                        var resume = [];
                        $.each(data, function (item_key, item) {
                            if (item && item[0]) {
                                if(has_stop_word($.trim(word), item[0])) resume.push(item_key);
                            }
                        });

                        if(resume.length && data.length){
                            $.each(resume, function (i, skip_key) {
                                if(data[skip_key]) {
                                    console.log('Skiped: ' + data[skip_key][0]);
                                    delete data[skip_key];
                                }
                            });

                            data = $.grep(data, function (a) {
                                return a !== undefined;
                            });
                        }
                    });
                }

                if(!data.length){
                    success_work();
                }

                $('#count').html(data.length);
                $('#key').html(get_item()).attr('title', get_item());
                update_iframe();
            }

            next_keys();

            $add.on('click', function (e) {
                e.preventDefault();
                send(true);
            });

            $cancel.on('click', function (e) {
                e.preventDefault();
                send(false);
            });

            $('.auto').on('click', function (e) {
                e.preventDefault();
                $('.interface').toggleClass('active');
            });

            $('#start').on('click', function (e) {
                e.preventDefault();
                auto_mode();
            });
        });
    </script>

<?php else:?>

    <form action="controllers/upload.php" method="post" enctype="multipart/form-data" accept="text/csv">
        <label for="data">
            File:
            <input onchange="$(this).closest('form').submit()" type="file" name="data">
        </label>
    </form>

<?php endif;?>

</body>
</html>