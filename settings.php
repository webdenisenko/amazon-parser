<?php
require_once("$_SERVER[DOCUMENT_ROOT]/classes/simple_html_dom.php");

function e($d){
	echo '<pre>';
	if(is_array($d) || is_object($d)) print_r($d);
	else echo $d;
	exit('</pre>');
}

function get_web_page( $url )
{
	$user_agent='Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';

	$options = array(

		CURLOPT_CUSTOMREQUEST  =>"GET",        //set request type post or get
		CURLOPT_POST           =>false,        //set to GET
		CURLOPT_USERAGENT      => $user_agent, //set user agent
		CURLOPT_COOKIEFILE     =>"cookie.txt", //set cookie file
		CURLOPT_COOKIEJAR      =>"cookie.txt", //set cookie jar
		CURLOPT_RETURNTRANSFER => true,     // return web page
		CURLOPT_HEADER         => false,    // don't return headers
		CURLOPT_FOLLOWLOCATION => true,     // follow redirects
		CURLOPT_ENCODING       => "",       // handle all encodings
		CURLOPT_AUTOREFERER    => true,     // set referer on redirect
		CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
		CURLOPT_TIMEOUT        => 120,      // timeout on response
		CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
	);

	$ch      = curl_init( $url );
	curl_setopt_array( $ch, $options );
	$content = curl_exec( $ch );
	$err     = curl_errno( $ch );
	$errmsg  = curl_error( $ch );
	$header  = curl_getinfo( $ch );
	curl_close( $ch );

	$header['errno']   = $err;
	$header['errmsg']  = $errmsg;
	$header['content'] = $content;
	return $header;
}

function utf8ize($d) {
	if (is_array($d)) {
		foreach ($d as $k => $v) {
			$d[$k] = utf8ize($v);
		}
	} else if (is_string ($d)) {
		return utf8_encode($d);
	}
	return $d;
}

function return_json($status, $data=[]){
	exit(json_encode([
		'status' => $status,
		'data' => $data
	]));
}

@define(DATA_FILE_PATH, "$_SERVER[DOCUMENT_ROOT]/data.csv");
@define(COMP_FILE_PATH, "$_SERVER[DOCUMENT_ROOT]/result.csv");
@define(STOP_FILE_PATH, "$_SERVER[DOCUMENT_ROOT]/stop.csv");
@define(FINISH_DIR, "$_SERVER[DOCUMENT_ROOT]/results");