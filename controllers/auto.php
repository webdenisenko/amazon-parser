<?php
include "$_SERVER[DOCUMENT_ROOT]/settings.php";
include "$_SERVER[DOCUMENT_ROOT]/classes/Parsing.php";

if(!isset($_POST['data']) || empty($_POST['data'])) exit('ERROR: DATA IS EMPTY');

$Parsing = new Parsing();

$data = $Parsing->auto(
	$_POST['data'],
	$_POST['brands'],
	$_POST['products']
);

if(count($data->good)){
	if(!file_exists(COMP_FILE_PATH)){
		$fp = fopen(COMP_FILE_PATH,"wb");
		fwrite($fp, 'phrase,volume,competitor(brands),competitor(products)'.$data->result->str);
		fclose($fp);
		$data->action = 'created';
	}else{
		file_put_contents(COMP_FILE_PATH, $data->result->str,FILE_APPEND | LOCK_EX);
		$data->action = 'added';
	}
}

return_json('success', $data);