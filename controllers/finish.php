<?php
include "$_SERVER[DOCUMENT_ROOT]/settings.php";

if(file_exists(COMP_FILE_PATH)){
	if(!file_exists(FINISH_DIR)) mkdir(FINISH_DIR, 0777);

	$data = file (DATA_FILE_PATH);
	if(count($data) == 1) return_json('empty');
	foreach ($data as &$item){
		$item = explode(',', $item);
	}

	function get_dir_name($name, $copy_num=0){
		$name = str_replace(' ', '_', $name);
		if(
			file_exists(FINISH_DIR . "/$name".($copy_num ? "($copy_num)" : '')) ||
			file_exists(FINISH_DIR . "/$name".($copy_num ? "($copy_num)" : '').".zip")
		) return get_dir_name($name, $copy_num+1);
		return $name.($copy_num ? "($copy_num)" : '');
	}

	$file_name = get_dir_name($data[1][0]);
	$dir = FINISH_DIR . "/$file_name";

	mkdir($dir, 0777);

	rename(COMP_FILE_PATH,  "$dir/complete.csv");
	rename(DATA_FILE_PATH,  "$dir/old.csv");

	$fp = fopen("$dir/stop.csv","wb");
	fwrite($fp, str_replace(',', '
', $_POST['stop']));
	fclose($fp);
}

$zip = new ZipArchive(); //Создаём объект для работы с ZIP-архивами
$zip->open("$dir/$file_name.zip", ZIPARCHIVE::CREATE); //Открываем (создаём) архив archive.zip

$zip->addFile("$dir/complete.csv", 'complete.csv'); //Добавляем в архив файл
$zip->addFile("$dir/stop.csv", 'stop.csv'); //Добавляем в архив файл
$zip->addFile("$dir/old.csv", 'old.csv'); //Добавляем в архив файл

$zip->close(); //Завершаем работу с архивом

unlink("$dir/complete.csv");
unlink("$dir/stop.csv");
unlink("$dir/old.csv");

rename("$dir/$file_name.zip",  FINISH_DIR . "/$file_name.zip");
@rmdir($dir);

return_json('success', str_replace($_SERVER['DOCUMENT_ROOT'], '', FINISH_DIR . "/$file_name.zip"));